#include <iostream>
#include <iomanip>

using namespace std;


int checkWin(int tic[3][3]) 
{
	//first player
	if (tic[0][0] == 1 && tic[0][1] == 1 && tic[0][2] == 1)
		return 1;
	if (tic[1][0] == 1 && tic[1][1] == 1 && tic[1][2] == 1)
		return 1;
	if (tic[2][0] == 1 && tic[2][1] == 1 && tic[2][2] == 1)
		return 1;

	if (tic[0][0] == 1 && tic[1][0] == 1 && tic[2][0] == 1)
		return 1;
	if (tic[0][1] == 1 && tic[1][1] == 1 && tic[2][1] == 1)
		return 1;
	if (tic[0][2] == 1 && tic[1][2] == 1 && tic[2][2] == 1)
		return 1;

	if (tic[0][0] == 1 && tic[1][1] == 1 && tic[2][2] == 1)
		return 1;
	if (tic[2][0] == 1 && tic[1][1] == 1 && tic[0][2] == 1)
		return 1;

	//second player
	if (tic[0][0] == 2 && tic[0][1] == 2 && tic[0][2] == 2)
		return 2;
	if (tic[1][0] == 2 && tic[1][1] == 2 && tic[1][2] == 2)
		return 2;
	if (tic[2][0] == 2 && tic[2][1] == 2 && tic[2][2] == 2)
		return 2;

	if (tic[0][0] == 2 && tic[1][0] == 2 && tic[2][0] == 2)
		return 2;
	if (tic[0][1] == 2 && tic[1][1] == 2 && tic[2][1] == 2)
		return 2;
	if (tic[0][2] == 2 && tic[1][2] == 2 && tic[2][2] == 2)
		return 2;

	if (tic[0][0] == 2 && tic[1][1] == 2 && tic[2][2] == 2)
		return 2;
	if (tic[2][0] == 2 && tic[1][1] == 2 && tic[0][2] == 2)
		return 2;

	return 0;
}


char tableValue(int value, char num) {
	if (value == 1) {
		return 'X';
	}
	else if (value == 2) {
		return 'O';
	}
	else if (value == 0) {
		return num;
	}
}

void table(int tic[3][3]) {
	char num = '1';
	cout << "           TIC-TAC-TOE        " << endl;
	for (int i = 0; i < 3; i++) {
		cout << "          _____________" << endl << "          ";
		for (int j = 0; j < 3; j++) {
			cout << "| " << tableValue(tic[i][j], num)<< " ";
			num++;
		}
		cout << "|" << endl;
	}
	cout << "          _____________" << endl;


}
void mark(int tic[3][3],int mark, int turn) {
	if (mark == 7) {
		tic[2][0] = turn;
	}
	else if (mark == 8) {
		tic[2][1] = turn;
	}
	else if (mark == 9) {
		tic[2][2] = turn;
	}
	else if (mark == 4) {
		tic[1][0] = turn;
	}
	else if (mark == 5) {
		tic[1][1] = turn;
	}
	else if (mark == 6) {
		tic[1][2] = turn;
	}
	else if (mark == 1) {
		tic[0][0] = turn;
	}
	else if (mark == 2) {
		tic[0][1] = turn;
	}
	else if (mark == 3) {
		tic[0][2] = turn;
	}

}

int checkTableValue(int tic[3][3], int mark) {
	if (mark == 7) {
		return tic[2][0];
	}
	else if (mark == 8) {
		return tic[2][1];
	}
	else if (mark == 9) {
		return tic[2][2];
	}
	else if (mark == 4) {
		return tic[1][0];
	}
	else if (mark == 5) {
		return tic[1][1];
	}
	else if (mark == 6) {
		return tic[1][2] ;
	}
	else if (mark == 1) {
		return tic[0][0];
	}
	else if (mark == 2) {
		return tic[0][1];
	}
	else if (mark == 3) {
		return tic[0][2];
	}
}

int AImark(int tic[3][3]) {
	int AIMark = 0;
	for (int testmark = 1; testmark < 10; testmark++) {
		int oldValue = checkTableValue(tic, testmark);
		//chech if it's empty and make it not to drop duplicate
		if (oldValue != 0) {
			continue;
		}
		mark(tic, testmark, 1);
		if (checkWin(tic)) {
			AIMark = testmark;
			mark(tic, testmark, oldValue);
			break;
		}
		else {
			mark(tic, testmark, oldValue);
		}
	}

	if (AIMark != 0) {
		return AIMark;
	}
	else {
		if (tic[1][1] == 0) {
			return 5;
		}
		else {
			for (int testmark = 1; testmark < 10; testmark++) {
				int oldValue = checkTableValue(tic, testmark);
				if (oldValue == 0) {
					AIMark = testmark;
					return  AIMark;
				}
			}
		}		
	}
}




int main()
{
	
	int tic[3][3] = {} ;
	int win = 0;
	table(tic);

	for (int i = 0; i < 9; i++) {
		int player;
		if (i % 2 == 0) { //player		
			cout << "Which box you want to drop? ";
			cin >> player;
			mark(tic, player,1);
			win = checkWin(tic);
			if (win != 0) {
				break;
			}
		}
		else { //AI
			player = AImark(tic);
			cout << "ujuuuu " << player;
			system("pause");
			mark(tic, player, 2);
			win = checkWin(tic);
			if (win != 0) {
				break;
			}
		}
		system("cls");
		table(tic);
	}

	system("cls");
	table(tic);

	if (win == 0) {
		cout << " TIE ";
	}
	else if(win == 1) {
		cout << " PLAYER 1 WIN ";
	}
	else if(win == 2) {
	cout << " PLAYER 2 WIN ";
	}

	return 0; 
}